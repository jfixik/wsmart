$(function(){

  $(window).on('load',function(){
    $('.partdos').hide();
  });


  $(document).each(function(){

    var messagealert = '<div class="modalmessageAlert" style="width:100%;height:50px;background:rgb(243, 60, 60);position:absolute;top:0px;z-index:999;">';
        messagealert += '<h2 style="font-size: 15px; position: relative; top: -3px; font-weight:bold; color: rgba(255, 255, 255, 0.91); margin-left: 20px; font-family: "Numans", sans-serif;">Al parecer hay un error, revisa bien porfavor!';
        messagealert += '<i class="icon-thumbs-down" style="margin-left:10px;"></i> <span class="icon-remove" id="close-alert" style="float: right;cursor:pointer; right: 20px; top: -2px; position: relative; font-size: 20px; color: rgba(230, 227, 227, 0.99);"></span></h2></div>';

      $(document).on('click','#next-getform',function(){
        var inputname = $('#name').val();
        var secondname = $('#secondname').val();
        if(inputname.length < 3 || secondname.length < 3){
            $('.modal-div').append(messagealert);
            return false;
        }else
          $('.modalmessageAlert').hide();
          console.log('all is ok');

        $(this).parent().animate({
          width : '700px'
        },'slow');
        $('.partone').css('opacity','.5');
        $('.partdos').delay(1000).fadeIn('slow');
      });

      $(document).on('click','.close',function(){
        $('.modal-div').hide();
      });
      $(document).on('click','#close-alert',function(){
        $('.modalmessageAlert').hide();
      });
   });
});